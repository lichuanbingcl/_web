module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    indent: [
			'error',
			4
		],
		'vue/html-indent': [
			'error',
			4,
			{
				attribute: 1,
				baseIndent: 1,
				closeBracket: 0,
				alignAttributesVertically: true,
				ignores: []
			}
		],
		'space-before-function-paren': [
			0,
			{
				anonymous: 'always',
				named: 'always',
				asyncArrow: 'always'
			}
		],
		'comma-dangle': [
			'error',
			'never'
		],
		'vue/max-attributes-per-line': [
			'error',
			{
				singleline: 3,
				multiline: {
					max: 1,
					allowFirstLine: false
				}
			}
		],
		'vue/html-self-closing': [
			'error',
			{
				html: {
					void: 'never',
					normal: 'always',
					component: 'always'
				},
				svg: 'always',
				math: 'always'
			}
		],
		'vue/require-v-for-key': 'error',
		'vue/component-tags-order': [
			'error',
			{
				order: [
					'template',
					'script',
					'style'
				]
			}
		],
		'vue/padding-line-between-blocks': [
			'error',
			'always'
		]
  }
}
