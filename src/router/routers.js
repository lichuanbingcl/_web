const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/layout',
        name: 'Layout',
        redirect: to => {
            const state = localStorage.getItem('islogin')
            if (state === '1') {
                return '/layout/papermanage'
            }
            if (state === '2') {
                return '/layout/paperlist'
            }
        },
        component: () =>
            import(/* webpackChunkName: "layout" */
                '@/views/layout/Layout.vue'),
        children: [
            // 公共组件
            {
                path: '/layout/userinfo',
                name: 'UserInfo',
                component: () =>
                    import(/* webpackChunkName: "userInfo" */
                        '@/views/layout/common/userInfo/UserInfo.vue')
            },
            {
                path: '/layout/resetpassword',
                name: 'ResetPassword',
                component: () =>
                    import(/* webpackChunkName: "ResetPassword" */
                        '@/views/layout/common/resetPassword/ResetPassword.vue')
            },
            // 学生组件
            {
                path: '/layout/paperlist',
                name: 'PaperList',
                component: () =>
                    import(/* webpackChunkName: "PaperList" */
                        '@/views/layout/student/paperList/PaperList.vue')
            },
            {
                path: '/layout/commitpaper',
                name: 'CommitPaper',
                component: () =>
                    import(/* webpackChunkName: "CommitPaper" */
                        '@/views/layout/student/commitPaper/CommitPaper.vue')
            },
            {
                path: '/layout/weeklynotes',
                name: 'WeeklyNotes',
                component: () =>
                    import(/* webpackChunkName: "WeeklyNotes" */
                        '@/views/layout/student/weeklyNotes/WeeklyNotes.vue')
            },
            // 教师组件
            {
                path: '/layout/grademanage',
                name: 'GradeManage',
                component: () =>
                    import(/* webpackChunkName: "GradeManage" */
                        '@/views/layout/teacher/gradeManage/GradeManage.vue')
            },
            {
                path: '/layout/papermanage',
                name: 'PaperManage',
                component: () =>
                    import(/* webpackChunkName: "PaperManage" */
                        '@/views/layout/teacher/paperManage/PaperManage.vue')
            },
            {
                path: '/layout/studentmanage',
                name: 'StudentManage',
                component: () =>
                    import(/* webpackChunkName: "StudentManage" */
                        '@/views/layout/teacher/studentManage/StudentManage.vue')
            },
            {
                path: '/layout/teachermanage',
                name: 'TeacherManage',
                component: () =>
                    import(/* webpackChunkName: "TeacherManage" */
                        '@/views/layout/teacher/teacherManage/TeacherManage.vue')
            }
        ]
    },
    {
        path: '/login',
        name: 'Login',
        component: () =>
            import(/* webpackChunkName: "login" */
                '@/views/login/Login.vue')
    },
    {
        path: '/404',
        name: '404',
        component: () =>
            import(/* webpackChunkName: "404" */
                '@/views/404/404.vue')
    }
]
export default routes
