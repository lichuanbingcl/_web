import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/less/index.js'// 引入样式文件
import apis from '@/apis' // 引入api文件
import components from '@/components/index.js'// 引入公共组件
import 'default-passive-events'// 引入阻止vue 鼠标事件控制台警告插件
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import dataFormat from '@/utils/dataFormat.js'

Vue.config.productionTip = false

// vue原型挂载 - 请求接口函数
Vue.prototype.$apis = apis

// 挂载 时间格式化函数
Vue.prototype.dataFormat = dataFormat

Vue.use(ElementUI)

// 全局注册公共组件
Vue.use(components)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

// 全局路由守卫
router.beforeEach((to, from, next) => {
    let islogin = localStorage.getItem('islogin')
    islogin = Boolean(Number(islogin))
    if (to.path.match('/layout')) {
        if (!islogin) {
            return next('/login')
        } else {
            return next()
        }
    } else {
        return next()
    }
})
