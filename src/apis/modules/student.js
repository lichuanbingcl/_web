import createAPI from '../fetch.js'

const STUDENT = {
    // 获取论文列表
    getPaperList: (data, $this, cancel) => createAPI('/student/studentPaperList', 'post', data, $this, cancel),
    // 修改密码
    resetStudentPassword: (data, $this, cancel) => createAPI('/student/studentPasswordUpdata', 'post', data, $this, cancel),
    // 获取学生信息
    getStudentInfo: (data, $this, cancel) => createAPI('/student/studentInfo', 'get', data, $this, cancel),
    // 修改学生信息
    commitStudentInfo: (data, $this, cancel) => createAPI('/student/editStudent', 'post', data, $this, cancel),
    // 选择论文
    checkPaper: (data, $this, cancel) => createAPI('/student/studentCheckedPaper', 'get', data, $this, cancel),
    // 所选论文
    myPaper: (data, $this, cancel) => createAPI('/student/selectStudentPaper', 'get', data, $this, cancel),
    // 添加周记
    addNote: (data, $this, cancel) => createAPI('/student/addStudentWeeklyNote', 'get', data, $this, cancel),
    // 获取周记列表
    getMyNoteList: (data, $this, cancel) => createAPI('/student/showStudentWeeklyNoteList', 'get', data, $this, cancel)
}
export default STUDENT
