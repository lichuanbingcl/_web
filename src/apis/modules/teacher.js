import createAPI from '../fetch.js'

const TEACHER = {
    // 获取我的论文列表
    getMyPaperList: (data, $this, cancel) => createAPI('/teacher/selectPaperList', 'post', data, $this, cancel),
    // 添加论文
    addPaper: (data, $this, cancel) => createAPI('/teacher/addPaper', 'post', data, $this, cancel),
    // 编辑论文
    editPaper: (data, $this, cancel) => createAPI('/teacher/editPaper', 'post', data, $this, cancel),
    // 删除论文
    deletePaper: (data, $this, cancel) => createAPI('/teacher/deletePaper', 'get', data, $this, cancel),
    //
    // 添加论文分数
    markPaperScore: (data, $this, cancel) => createAPI('/teacher/addPaperScore', 'get', data, $this, cancel),
    // 获取论文分数列表
    getPaperScoreList: (data, $this, cancel) => createAPI('/teacher/showPaperScoreList', 'post', data, $this, cancel),
    //
    // 添加周记评价
    addNoteEvaluate: (data, $this, cancel) => createAPI('/teacher/editNote', 'get', data, $this, cancel),
    // 获取周记列表
    getNotesList: (data, $this, cancel) => createAPI('/teacher/showStudentWeeklyNoteList', 'get', data, $this, cancel),
    //
    // 获取教师信息
    getTeacherInfo: (data, $this, cancel) => createAPI('/teacher/teacherInfo', 'get', data, $this, cancel),
    // 修改密码
    resetTeacherPassword: (data, $this, cancel) => createAPI('/teacher/teacherPasswordUpdate', 'post', data, $this, cancel),
    //
    // 获取学生列表
    getStudentList: (data, $this, cancel) => createAPI('/teacher/selectStudentList', 'post', data, $this, cancel),
    // 添加学生信息
    addStudent: (data, $this, cancel) => createAPI('/teacher/addStudent', 'post', data, $this, cancel),
    // 删除学生信息
    deleteStudent: (data, $this, cancel) => createAPI('/teacher/deleteStudent', 'get', data, $this, cancel),
    // 修改学生信息
    editStudent: (data, $this, cancel) => createAPI('/teacher/editStudent', 'post', data, $this, cancel),
    //
    // 获取教师列表
    getTeacherList: (data, $this, cancel) => createAPI('/teacher/selectTeacherList', 'post', data, $this, cancel),
    // 添加教师信息
    addTeacher: (data, $this, cancel) => createAPI('/teacher/addTeacher', 'post', data, $this, cancel),
    // 删除教师信息
    deleteTeacher: (data, $this, cancel) => createAPI('/teacher/deleteTeacher', 'get', data, $this, cancel),
    // 修改教师信息
    editTeacher: (data, $this, cancel) => createAPI('/teacher/editTeacher', 'post', data, $this, cancel)
}
export default TEACHER
