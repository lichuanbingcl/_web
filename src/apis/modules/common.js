import createAPI from '../fetch.js'

const COMMON = {
    // 登录
    login: (data, $this, cancel) => createAPI('/user/login', 'post', data, $this, cancel),
    // 退出
    exit: (data, $this, cancel) => createAPI('/user/logout', 'post', data, $this, cancel),
    // 文件上传
    upload: (data, $this, cancel) => createAPI('/file/upload', 'post', data, $this, cancel),
    // 获取文件列表
    getFilesList: (data, $this, cancel) => createAPI('/file/fileList', 'get', data, $this, cancel)

}
export default COMMON
