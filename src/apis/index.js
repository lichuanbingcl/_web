import COMMON from './modules/common'
import STUDENT from './modules/student'
import TEACHER from './modules/teacher'

const apis = {
    ...COMMON,
    ...STUDENT,
    ...TEACHER

}
export default apis
