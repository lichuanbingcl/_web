const dataFormat = {
    formatterDate (value) {
        const date = new Date(parseInt(value * 1000))
        const Y = date.getFullYear()
        const M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
        const D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
        const H = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
        const m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
        const S = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
        return Y + '-' + M + '-' + D + '  ' + H + ':' + m + ':' + S
    }
}

export default dataFormat
