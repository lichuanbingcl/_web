import ItemHeader from '@/components/common/ItemHeader.vue'

function components(Vue) {
    Vue.component('item-header', ItemHeader)
}
export default components
