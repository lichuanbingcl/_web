module.exports = {
    devServer: {
    // 配置自动启动浏览器
        open: true,
        // 主机名
        host: 'localhost',
        // 端口号
        port: 8080,
        // 支持https，使用自签名证书
        https: false,
        // 模块热替换
        hot: true,
        // 热更新会刷新页面
        hotOnly: false,
        // 当出现编译器错误或警告时，在浏览器中显示
        overlay: {
            warnings: true,
            errors: true
        }
    },
    publicPath: '/', // 根域上下文目录
    outputDir: 'dist', // 构建输出目录
    assetsDir: 'assets', // 静态资源目录 (js, css, img, fonts)
    lintOnSave: true, // 是否开启eslint保存检测，有效值：ture | false | 'error'
    runtimeCompiler: true, // 运行时版本是否需要编译
    transpileDependencies: [], // 默认babel-loader忽略mode_modules，这里可增加例外的依赖包名
    productionSourceMap: false
}
